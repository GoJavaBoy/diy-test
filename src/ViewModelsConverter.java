import to.MaterialTo;
import to.RootModelTo;
import to.TechModelTo;

import java.util.LinkedList;
import java.util.List;

public class ViewModelsConverter {

    // Сюда приходит список:  TECHNOLOGY,MATERIAL,MATERIAL,..,MATERIAL
    public static TechModelTo toTechModel(List<RowModel> source) {
        TechModelTo result = null;
        for (RowModel rowModel : source) {
            switch (rowModel.positionType) {
                case ROOT -> throw new IllegalArgumentException();
                case TECHNOLOGY -> {
                    if (result != null) throw new IllegalArgumentException();
                    result = new TechModelTo(rowModel.anyCode);
                }
                case MATERIAL -> {
                    if (result == null) throw new IllegalArgumentException();
                    result.rowTos.add(new MaterialTo(rowModel.anyCode));
                }
            }
        }
        return result;
    }

    // Сюда приходит список:  ROOT,TECHNOLOGY,MATERIAL,MATERIAL,..,MATERIAL, [ROOT,TECHNOLOGY,MATERIAL,MATERIAL,..,MATERIAL], ...
    public static List<RootModelTo> toRootModels(List<RowModel> source) {
        LinkedList<RootModelTo> roots = new LinkedList<>();
            for (RowModel rowModel : source) {
                switch (rowModel.positionType) {
                    case ROOT -> {
                        roots.add(new RootModelTo(rowModel.anyCode));
                    }
                    case TECHNOLOGY -> {
                        if (roots.isEmpty()) throw new IllegalArgumentException();
                        roots.getLast().techList.add(new TechModelTo(rowModel.anyCode));
                    }
                    case MATERIAL -> {
                        if (roots.isEmpty()) throw new IllegalArgumentException();
                        List<TechModelTo> techModelToList = roots.getLast().techList;
                        if (techModelToList.isEmpty()) throw new IllegalArgumentException();
                        TechModelTo techModelTo = techModelToList.get(techModelToList.size() - 1);
                        techModelTo.rowTos.add(new MaterialTo(rowModel.anyCode));
                    }
                }
            }
        return roots;
    }
}
